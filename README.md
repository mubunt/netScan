
# APPLICATION: *netScan*

 **netScan**  is a very simple LINUX application allowing to display the peripherals (computers, phones, tablets, plugs, etc.) connected to your local network. This application has been developed strictly for a  personal / family environment and therefore has nothing to do with sophisticated professional or semi-professional applications like *SoftPerfect Network Scanner*, *Advanced IP Scanner*, or the excellent *Angry IP Scanner*.
 
 **Example:**
 ![netwokScan interface](README_images/netScan-00.png  "netScan interface")
 
The network is periodically scanned  and the list is updated accordingly.

## LICENSE
**netScan** is covered by the GNU General Public License (GPL) version 3 and above.

## USAGE

``` Bash
	$ ./linux/netScan --version
	netScan - Copyright (c) 2016-2022, Michel RIZZO. All Rights Reserved.
	netScan - Version 2.0.0	
	$ ./linux/netScan --help
	Usage: netScan [OPTION]...
	A local network scanner

		-h, --help           Print help and exit
		-V, --version        Print version and exit
		-v, --verbose        Verbose mode.  (default=off)
		-D, --delay=INTEGER  Delay in seconds between 2 scans. Default is 60 seconds.

	To be run with ROOT permission (SUDO).
	Exit: returns a non-zero status if an error is detected.

	$ sudo ./linux/netScan --verbose --delay=20
```

## STRUCTURE OF THE APPLICATION
This section walks you through **netScan**'s structure. Once you understand this structure, you will easily find your way around in **netScan**'s code base.

``` Bash

$ yaTree
./                           # Application level
├── README_images/           # Images for documentation
│   └── netScan-00.png       # 
├── netScan_bin/             # Binary directory: jars (third-parties and local) and driver
│   ├── Makefile             # -- Makefile
│   ├── commons-cli-1.4.jar  # -- Third-party COMMONS CLI 1.4 jar file
│   └── swt-linux-425.jar    # -- Third-party SWT jar file
├── netScan_c/               # C Source directory
│   ├── Makefile             # -- Makefile
│   ├── netScan.c            # -- C main source file
│   └── netScan.ggo          # -- 'gengetopt' option definition. SRefer to https://www.gnu.org/software/gengetopt/gengetopt.html
├── netScan_java/            # JAVA Source directory
│   ├── netScan/             # -- IntelliJ infoVault project structure
│   │   ├── src/             # 
│   │   │   ├── netCmd.java  # 
│   │   │   ├── netGui.java  # 
│   │   │   └── netScan.java # 
│   │   └── netScan.iml      # 
│   └── Makefile             # -- Makefile
├── COPYING.md               # GNU General Public License markdown file
├── LICENSE.md               # License markdown file
├── Makefile                 # Top-level makefile
├── README.md                # ReadMe Mark-Down file
├── RELEASENOTES.md          # Release Notes markdown file
└── VERSION                  # Version identification text file

6 directories, 18 files
$
```

## HOW TO BUILD THIS APPLICATION
``` Bash
	$ cd netScan
	$ make clean all
```

## HOW TO INSTALL AND USE THIS APPLICATION

``` Bash
	$ cd netScan
	$ make clean all
	$ sudo ./linux/netScan

	or

	$ make release INSTALLDIR=<Path-to-installation-directory>
		# Executable generated with -O2 option, is installed in $BIN_DIR directory (defined at environment level).
		# We consider that $BIN_DIR is a part of the PATH.
	$ sudo <Path-to-installation-directory>/netScan <options>
```

## HOW TO PLAY WITH JAVA SOURCES
To play with, we recommend to use **IntelliJ IDEA**, on Linux platform:

- Launch IntelliJ IDEA
- Click on **File -> Open...** and select *netScan/netScan_java/netScan* project
- It's your turn...

## SOFTWARE REQUIREMENTS
- For usage:
  - JAVA 1.11.0 for usage and development
  - Command *arp-scan*:  The ARP scanner.
- For development:
  - IntelliJ IDEA 2022.3 (Community Edition)
      Build #IC-223.7571.182, built on November 29, 2022
      Runtime version: 17.0.5+1-b653.14 amd64
      VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
  - Openjdk:
      openjdk 11.0.17 2022-10-18
      OpenJDK Runtime Environment (build 11.0.17+8-post-Ubuntu-1ubuntu2)
      OpenJDK 64-Bit Server VM (build 11.0.17+8-post-Ubuntu-1ubuntu2, mixed mode)
  - Javac:
      javac 11.0.17
  - GNU gengetopt 2.23

Application developed and tested with XUBUNTU 22.10.

## RELEASE NOTES
Refer to file [RELEASENOTES](RELEASENOTES.md) .

***
