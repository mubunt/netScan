# RELEASE NOTES: *netkcan*, a local network scanner

Functional limitations, if any, of this version are described in the *README.md* file.

- **Version 2.0.0**:
  - JAVA: update SWT jar file from version 4.15 to version 4.25
  - JAVA: Version management is done at project level (no more need of netBuildInfo.java)
  - JAVA: use of "arp-scan" utility instead of the previous set of commands (nmap, ping, etc.)
  - C: check if the "arp-scan" utility is installed
  - C: various source enhancements
  - C and JAVA: remove the physical hardware address file

- **Version 1.4.5**:
  - Updated build system components.

- **Version 1.4.4**:
  - Updated build system.

- **Version 1.4.3**:
  - Removed unused files.

- **Version 1.4.2**:
  - Updated build system component(s)

- **Version 1.4.1**:
  - Updated SWT library from 4.12 to 4.15.
  - Reworked build system to ease global and inter-project updated.
  - Added *cppcheck* target (Static C code analysis) and run it.

- **Version 1.4.0**:
  - Removed build method for Windows and associated files.

- **Version 1.3.1**:
  - Some minor changes in project structure and build

- **Version 1.3.0**:
  - Improved ./Makefile, ./netScan_bin/Makefile, ./netScan_c/Makefile, ./netScan_java/Makefile.
  - Removed C compilation warnings.
  - Moved to SWT 4.12. Initially was 4.8.
  - Updated README file.
  - Added some verbose trace in java program to be able to best survey whit it idoes :-).

- **Version 1.2.0**:
  - Removed generated jar files on *clean*.
  - Automated, as much as possible, the management of java libraries to facilitate their update.
  - Added version identification text file.
  - Updated java libraries to their most recent versions.
  - Moved from GPL v2 to GPL v3.
  - Replaced license files (COPYNG and LICENSE) by markdown version.
  - Replaced Release Nores file (this file) by markdown version.
  - Removed *index.html* file (was used for *GitHub*).

- **Version 1.1.0**:
  - Improved user interface.
  - Added "force" button.

- **Version 1.0.0**:
  - First release.
