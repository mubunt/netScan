#------------------------------------------------------------------------------
# Copyright (c) 2020-2022, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	JAVA jar generation and processing for C/Java projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		PROGRAM 		Name of the jar to generate
#		VERSION 		Version of the jar to generate (come from top level makefile)
#		IDEA_PRJ 		Name of the IDEA project
#		BINJAR 			Relative path to directory where are stored jar files
#		SOURCES 		List of java source files (except the 'version' source)
#		JARS 			List of jar dependencies
#------------------------------------------------------------------------------
IDEA_BIN	= $(IDEA_PRJ)/out/production/$(IDEA_PRJ)
JARFILE		= $(BINJAR)/$(PROGRAM).jar
CLASSES 	= $(addprefix $(IDEA_BIN)/,$(notdir $(SOURCES:.java=.class)))
MANIFESTTMP	= manifest.txt
#-------------------------------------------------------------------------------
RM			= rm -f
RMDIR		= rm -fr
MKDIR		= mkdir -p
INSTALL		= install -p -v -D
JC 			= javac
CAT 		= cat
SED 		= sed -e
JCFLAGS		= -d $(IDEA_BIN) -cp "$(BINJAR)/*"
JAR 		= jar
JARFLAGS	= cmf

.SUFFIXES: .java .class
#-------------------------------------------------------------------------------
$(CLASSES): $(SOURCES)
	@echo "-- Compiling java sources"
	$(MUTE)$(MKDIR) $(IDEA_BIN)
	$(MUTE)$(JC) $(JCFLAGS) $(SOURCES)
$(JARFILE): $(CLASSES)
	@echo "-- Creating MANIFEST source file"
	$(MUTE)cd $(IDEA_BIN); \
		echo "Manifest-Version: 1.0" > $(MANIFESTTMP); \
		echo "Class-Path: $(JARS) " >> $(MANIFESTTMP); \
		echo "Main-Class: $(PROGRAM)" >> $(MANIFESTTMP); \
		echo "" >> $(MANIFESTTMP); \
		echo "-- Compiling and creating JAR file $(JARFILE)"; \
		$(JAR) $(JARFLAGS) $(MANIFESTTMP) ../../../../$(JARFILE) *.class; \
		$(RM) $(MANIFESTTMP)
#-------------------------------------------------------------------------------
all: $(JARFILE)
clean:
	@echo "-- Removing JAR file $(JARFILE)"
	$(MUTE)$(RM) $(JARFILE)
	@echo "-- Removing CLASS files"
	$(MUTE)$(RMDIR) $(IDEA_PRJ)/out
#-------------------------------------------------------------------------------
.PHONY: all clean
#-------------------------------------------------------------------------------