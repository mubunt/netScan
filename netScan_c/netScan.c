//------------------------------------------------------------------------------
// Copyright (c) 2016-2022, Michel RIZZO (the 'author' in the following)
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <libgen.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <zconf.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
//------------------------------------------------------------------------------
// MACROS DEFINITIONS TO CUSTOMIZE THE PROGRAM
//------------------------------------------------------------------------------
#define BINDIR		"/netScan_bin/"
#define NETSCAN 	"netScan.jar"
#define HEADER		"netScan_cmdline.h"
#define PARSER 		cmdline_parser_netScan
#define FREE 		cmdline_parser_netScan_free
//------------------------------------------------------------------------------
// APPLICATION HEADER FILE
//------------------------------------------------------------------------------
#include HEADER
//------------------------------------------------------------------------------
// MACROS DEFINITIONS
//------------------------------------------------------------------------------
#define DISPLAY 				"DISPLAY"
#define JAVA 					"java"
#define ARPSCAN					"arp-scan"
#define ProcessPseudoFileSystem	"/proc/self/exe"
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#define error(fmt, ...) 		do { fprintf(stderr, "\nERROR: " fmt "\n\n", __VA_ARGS__); } while (0)
#define EXIST(x)				(stat(x, ptlocstat)<0 ? 0:locstat.st_mode)
#define EXISTDIR(y)				(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)
//------------------------------------------------------------------------------
// GLOBAL VARIABLES
//------------------------------------------------------------------------------
static char 		PathOfAssociatedJarFiles[PATH_MAX];
static struct stat	locstat, *ptlocstat;        // stat variables for EXIST*
//------------------------------------------------------------------------------
// LOCAL ROUTINES
//------------------------------------------------------------------------------
static bool can_run_command(const char *cmd, char *realcmd) {
	if(strchr(cmd, '/')) {
		// if cmd includes a slash, no path search must be performed,
		// go straight to checking if it's executable
		strcpy(realcmd, cmd);
		return access(cmd, X_OK) == 0;
	}
	const char *path = getenv("PATH");
	if (!path) return false; // something is horribly wrong...
	// we are sure we won't need a buffer any longer
	char *buf = malloc(strlen(path)+strlen(cmd)+3);
	if (!buf) return false; // actually useless, see comment
	// loop as long as we have stuff to examine in path
	for (; *path; ++path) {
		// start from the beginning of the buffer
		char *p = buf;
		// copy in buf the current path element
		for (; *path && *path!=':'; ++path,++p) *p = *path;
		// empty path entries are treated like "."
		if (p==buf) *p++='.';
		// slash and command name
		if (p[-1]!='/') *p++='/';
		strcpy(p, cmd);
		// check if we can execute it
		if (access(buf, X_OK)==0) {
			strcpy(realcmd, buf);
			free(buf);
			return true;
		}
		// quit at last cycle
		if (!*path) break;
	}
	// not found
	free(buf);
	return false;
}
//------------------------------------------------------------------------------
static bool testJARfile(char *jarFile) {
	char jarfile[PATH_MAX];
	snprintf(jarfile, sizeof(jarfile), "%s%s", PathOfAssociatedJarFiles, jarFile);
	if (! EXIST(jarfile)) {
		error("WRONG INSTALLATION - Jar file '%s' does not exist.", jarfile);
		return false;
	}
	return true;
}
//------------------------------------------------------------------------------
// MAIN FUNCTION
//
// Return code (EXIT_SUCCESS - Normal program stop / EXIT_FAILURE - Program stop on error)
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	struct gengetopt_args_info	args_info;
	char command[4096 * 4];
	//---- Initialization ------------------------------------------------------
	ptlocstat = &locstat;
	//---- Get location of this executable -------------------------------------
	if (readlink(ProcessPseudoFileSystem, PathOfAssociatedJarFiles, sizeof(PathOfAssociatedJarFiles)) == -1) {
		error("%s", "Cannot get path of the current executable.");
		return EXIT_FAILURE;
	}
	dirname(PathOfAssociatedJarFiles);
	strcat(PathOfAssociatedJarFiles, BINDIR);
	//---- Check consistency of the installation -------------------------------
	if (! EXISTDIR(PathOfAssociatedJarFiles)) {
		error("WRONG INSTALLATION - Directory '%s' does not exist.", PathOfAssociatedJarFiles);
		return EXIT_FAILURE;
	}
	if (! testJARfile((char *)NETSCAN)) return EXIT_FAILURE;
	if (! testJARfile((char *)COMMONSCLI)) return EXIT_FAILURE;
	if (! testJARfile((char *)SWT)) return EXIT_FAILURE;
	char realcmd[PATH_MAX] = "";
	if (! can_run_command(ARPSCAN, realcmd)) {
		error("WRONG INSTALLATION - Application '%s' is not installed.", ARPSCAN);
		return EXIT_FAILURE;
	}
	//---- Check consistency of the environment --------------------------------
	if (NULL == getenv(DISPLAY)) {
		error("%s", "WRONG ENVIRONMENT - DISPLAY not initialized.");
		return EXIT_FAILURE;
	}
	//---- Parameter checking and setting --------------------------------------
	if (PARSER(argc, argv, &args_info) != 0)
		return EXIT_FAILURE;
	//---- Check if this executable is run as root user (sudo) -----------------
	if (getuid() != 0) {	//or you can use  a=geteuid(); both euid and uid are zero when you are root user
		error("%s", "Please run the executable as root user.");
		return EXIT_FAILURE;
	}
	//---- Run Java
	char *pt;
	size_t k;
	sprintf(command, "%s -cp %s -jar %s%s", JAVA, PathOfAssociatedJarFiles, PathOfAssociatedJarFiles, NETSCAN);
	for (int j = 1; j < argc; j++) {
		strcat(command, " ");
		pt = argv[j];
		k = strlen(command);
		while (*pt != '\0') {
			if (*pt == ' ') command[k++] = '\\';
			command[k] = *pt;
			command[++k] = '\0';
			++pt;
		}
	}
	int retval = system(command);
	//---- End -------------------------------------------------------------------
	FREE(&args_info);
	return(retval);
}
//------------------------------------------------------------------------------
