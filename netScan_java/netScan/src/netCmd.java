//------------------------------------------------------------------------------
// Copyright (c) 2017-2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

class netCmd {
	private static final String cmd_arpscan = "arp-scan --localnet --plain";
	//--------------------------------------------------------------------------
	static void scan(boolean scanType) {
		if (scanType == netScan.forcedScan)
			netScan.Verbose("Forced network scanning");
		else
			netScan.Verbose("Scheduled network scanning");

		for (int i = 0; i < netScan.numberOfConnections; i++)
			netScan.devices[i].resetconnected();

		String[] output = executeCommand(cmd_arpscan);
			//192.168.0.1	e4:9e:12:82:a2:87	FREEBOX SAS
			//192.168.0.9	00:24:d4:71:72:a4	FREEBOX SAS
			//192.168.0.5	22:d8:9b:63:c8:8a	(Unknown: locally administered)
			//192.168.0.3	2c:d0:5a:4b:66:a1	Liteon Technology Corporation
			//192.168.0.2	e0:24:81:c0:38:36	HUAWEI TECHNOLOGIES CO.,LTD

		for (String line : output) {
			if (line.length() == 0) continue;
			String[] words = line.split("\t");
			String[] ar = words[0].split("\\.");
			int i = Integer.parseInt(ar[3]);
			netScan.devices[i].setipaddr(words[0]);
			netScan.devices[i].setmacaddr(words[1]);
			netScan.devices[i].setvendor(words[2]);
			netScan.devices[i].setconnected();
		}
		String[] ar = netScan.myIPAddress.split("\\.");
		int i = Integer.parseInt(ar[3]);
		netScan.devices[i].setipaddr(netScan.myIPAddress);
		netScan.devices[i].setmacaddr(netScan.myMACAddr);
		netScan.devices[i].setvendor(netScan.myVendor);
		netScan.devices[i].setconnected();
	}
	//--------------------------------------------------------------------------
	private static String[] executeCommand(String command) {
		ArrayList<String> output = new ArrayList<>();
		try {
			Process p = Runtime.getRuntime().exec(command);
			p.waitFor();
			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.length() != 0) { output.add(line); }
			}
		} catch (Exception e) {
			netScan.Error("Execution error: " + e.getMessage());
		}
		String[] result = new String[output.size()];
		output.toArray(result);
		return result;
	}
}
//==============================================================================
