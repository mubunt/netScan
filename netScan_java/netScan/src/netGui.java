//------------------------------------------------------------------------------
// Copyright (c) 2017-2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import java.text.SimpleDateFormat;
import java.util.Calendar;

class netGui implements Runnable {
	static Display display = null;
	static Font fontConsole = null;
	static Font fontButton = null;
	static Font fontLabel = null;
	private static Shell shell = null;
	private static StyledText console;
	private static Button force = null;
	private static Label status = null;
	private static final String eol = System.getProperty("line.separator");
	//--------------------------------------------------------------------------
	@Override
	public void run() {
		display = new Display();
		fontConsole = new Font(display, "Arial", 10, SWT.NORMAL);
		fontButton = new Font(display, "Arial", 10, SWT.BOLD);
		fontLabel = new Font(display, "Arial", 10, SWT.ITALIC);

		shell = new Shell(display, SWT.CLOSE | SWT.TITLE | SWT.RESIZE);
		shell.setLayout(new FormLayout());
		shell.setText(netScan.sTitle);
		shell.addListener(SWT.Close, event -> netScan.Exit(0));
		//---
		FormData fd1 = new FormData();
		fd1.height = 35;
		fd1.top = new FormAttachment(shell,0, -5);
		fd1.left = new FormAttachment(0, 5);
		fd1.right = new FormAttachment(80, -5);
		status = new Label(shell, SWT.LEFT);
		status.setLayoutData(fd1);
		status.setFont(fontLabel);
		status.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		displayStatus("Initializing...");
		//---
		FormData fd2 = new FormData();
		fd2.top = new FormAttachment(status, 0, SWT.TOP);
		fd2.bottom = new FormAttachment(status, 0, SWT.BOTTOM);
		fd2.right = new FormAttachment(100, -5);
		force = new Button(shell, SWT.PUSH);
		force.setLayoutData(fd2);
		force.setFont(fontButton);
		force.setText("Force");
		force.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		force.setBackground(Display.getCurrent().getSystemColor(SWT.COLOR_RED));
		switchOffButtons();
		force.addSelectionListener(new SelectionAdapter() {
			public void widgetSelected(SelectionEvent event) {
				netScan.scanandupdate(netScan.forcedScan);
			}
		});
		//---
		FormData fd3 = new FormData();
		fd3.height = 20;
		fd3.top = new FormAttachment(status, 0, SWT.BOTTOM);
		fd3.left = new FormAttachment(0);
		fd3.right = new FormAttachment(100);
		Label headers = new Label(shell, SWT.LEFT);
		headers.setLayoutData(fd3);
		headers.setFont(fontConsole);
		headers.setForeground(Display.getCurrent().getSystemColor(SWT.COLOR_BLACK));
		headers.setText("  #    IP Address        H/W Address         Vendor");
		//---
		FormData fd4 = new FormData();
		fd4.top = new FormAttachment(headers, 0, SWT.BOTTOM);
		fd4.left = new FormAttachment(0);
		fd4.right = new FormAttachment(100);
		fd4.bottom = new FormAttachment(100);
		console = new StyledText(shell, SWT.NONE);
		console.setLayoutData(fd4);
		console.setForeground(display.getSystemColor(SWT.COLOR_WHITE));
		console.setBackground(new Color(display, 59, 59, 59));	// grey23
		console.setFont(fontConsole);
		console.setEditable(false);
		console.setEnabled(true);
		//---
		shell.pack();
		shell.layout(true, true);
		shell.setSize(500, 400);
		shell.setLocation((display.getBounds().width - shell.getBounds().width) / 2,
				(display.getBounds().height - shell.getBounds().height) / 2);
		shell.open();
		while (!shell.isDisposed())
			if (!display.readAndDispatch()) display.sleep();
		netScan.Exit(0);
	}
	//--------------------------------------------------------------------------
	static void update() {
		display.syncExec(() -> {
			switchOffButtons();
			Calendar cal= Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			String title = "Connected devices at " + sdf.format(cal.getTime())
					+ " - Scan delay="  + netScan.delay / 1000 + " sec.";
			displayStatus(title);
			displayConnection();
			switchOnButtons();
		});
	}
	//--------------------------------------------------------------------------
	private static void switchOnButtons() {
		force.setEnabled(true);
	}
	private static void switchOffButtons() {
		force.setEnabled(false);
	}
	private static void displayStatus(String label) {
		status.setText(label);
	}
	private static void clearConsole() { console.setText(""); }
	//--------------------------------------------------------------------------
	private static void displayConnection() {
		clearConsole();
		int line = 0;
		for (int i = 0; i < netScan.numberOfConnections; i++)
			if (netScan.devices[i].getconnected()) {
				String s = String.format("%3d\t\t", line) +
						String.format("%-15s\t", netScan.devices[i].getipaddr()) +
						String.format("%-17s\t", netScan.devices[i].getmacaddr()) +
						String.format("%s", netScan.devices[i].getvendor());
				console.append(s + eol);
				line++;
			}
		int k = 35;	// 30 pixels per line
		shell.layout(true, true);
		shell.setSize(shell.getBounds().width, line * k + 20);
	}
}
//==============================================================================
