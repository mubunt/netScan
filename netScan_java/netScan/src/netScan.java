//------------------------------------------------------------------------------
// Copyright (c) 2017-2022, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

import org.apache.commons.cli.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Enumeration;

@SuppressWarnings("InfiniteLoopStatement")
public class netScan {
	private static final String sOuiFile = "/var/lib/ieee-data/oui.txt";
	private static final String sVerbose = "[VERBOSE] ";
	private static final String sError = "!!! ERROR !!! ";
	private static final String sExit = "EXIT...";
	private static final int delay_default = 60000;	// milliseconds = 60 seconds
	private static final boolean scheduledScan = false;
	private static final Object lock = new Object();
	static boolean verboseMode = false;
	static final String sTitle = "Local Network Scanner";
	static String myIPAddress = null;
	static String myMACAddr = null;
	static String myVendor = null;
	static final boolean forcedScan	= true;
	static int delay = delay_default;
	static class device {
		String ipaddress;
		String macaddress;
		String vendor;
		boolean connected;
		String getipaddr() { return this.ipaddress; }
		void setipaddr(String address) { this.ipaddress = address; }
		String getmacaddr() { return this.macaddress; }
		void setmacaddr(String address) { this.macaddress = address; }
		String getvendor() { return this.vendor; }
		void setvendor(String vendor) { this.vendor = vendor; }
		boolean getconnected() { return this.connected; }
		void setconnected() { this.connected = true; }
		void resetconnected() { this.connected = false; }
	}
	static final int numberOfConnections = 256;		// max number of connection per network
	static final device[] devices = new device[numberOfConnections];
	//--------------------------------------------------------------------------
	public static void main(String[] args) throws UnknownHostException {
		// OPTIONS PROCESSING
		CommandLineParser parser = new DefaultParser();
		Options options = new Options();
		options.addOption("v", "verbose", false,"verbose mode");
		options.addOption("D", "delay",   true, "Delay in seconds between 2 scans. Default is " + delay_default + " seconds");
		try {
			CommandLine line = parser.parse(options, args);
			// Option: Verbose
			if (line.hasOption("verbose"))
				verboseMode = true;
			// Option: Delay
			if (line.hasOption("delay")) {
				String sdelay = line.getOptionValue("delay");
				try {
					delay = Integer.parseInt(sdelay) * 1000;	// Seconds to milliseconds
				} catch (NumberFormatException nfe) {
					Error("'" + sdelay + "' is not a valid number");
				}
			}
		} catch (ParseException exp) { Error("Parsing failed - " + exp.getMessage()); }
		//----------------------------------------------------------------------
		// MY IP ADDRESS
		myIPAddress = getMyLANAddress();
		if ("127.0.1.1".equals(myIPAddress) || !validIP(myIPAddress)) {
			Error("It seems that there is no TCP/IP connection....");
		}
		// MY MAC ADDRESS
		myMACAddr = getMyMACAddress(myIPAddress);
		// MY VENDOR
		File f = new File(sOuiFile);
		if (f.isFile()) {
			myVendor = grep(myMACAddr);
		} else {
			myVendor = "(Unknown: locally administered - " + InetAddress.getLocalHost().getHostName() + ")";
		}
		//----------------------------------------------------------------------
		// GO ON....
		for (int k = 0; k < numberOfConnections; k++) {
			devices[k] = new device();
			devices[k].setipaddr("");
			devices[k].setmacaddr("");
			devices[k].setvendor("");
			devices[k].resetconnected();
		}

		final netGui gui = new netGui();
		Thread t = new Thread(gui);
		t.start();
		try { Thread.sleep(1000); } catch (InterruptedException e) { Error("Interrupted Exception: '" + e.getMessage()); }
		do {
			scanandupdate(scheduledScan);
			try { Thread.sleep(delay); } catch (InterruptedException e) { Error("Interrupted Exception: '" + e.getMessage()); }
		} while (true);
	}
	//--------------------------------------------------------------------------
	static void scanandupdate(boolean scanType) {
		synchronized(lock) {
			netCmd.scan(scanType);
			netGui.update();
		}
	}
	//--------------------------------------------------------------------------
	/**
	 * Returns an <code>InetAddress</code> object encapsulating what is most likely the machine's LAN IP address.
	 * <p/>
	 * This method is intended for use as a replacement of JDK method <code>InetAddress.getLocalHost</code>, because
	 * that method is ambiguous on Linux system. Linux systems enumerate the loopback network interface the same
	 * way as regular LAN network interfaces, but the JDK <code>InetAddress.getLocalHost</code> method does not
	 * specify the algorithm used to select the address returned under such circumstances, and will often return the
	 * loopback address, which is not valid for network communication. Details
	 * <a href="http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4665037">here</a>.
	 * <p/>
	 * This method will scan all IP addresses on all network interfaces on the host machine to determine the IP address
	 * most likely to be the machine's LAN address. If the machine has multiple IP addresses, this method will prefer
	 * a site-local IP address (e.g. 192.168.x.x or 10.10.x.x, usually IPv4) if the machine has one (and will return the
	 * first site-local address if the machine has more than one), but if the machine does not hold a site-local
	 * address, this method will return simply the first non-loopback address found (IPv4 or IPv6).
	 * <p/>
	 * If this method cannot find a non-loopback address using this selection algorithm, it will fall back to
	 * calling and returning the result of JDK method <code>InetAddress.getLocalHost</code>.
	 * <p/>
	 */
	private static String getMyLANAddress() {
		try {
			InetAddress candidateAddress = null;
			// Iterate all NICs (network interface cards)...
			for (Enumeration ifaces = NetworkInterface.getNetworkInterfaces(); ifaces.hasMoreElements(); ) {
				NetworkInterface iface = (NetworkInterface) ifaces.nextElement();
				// Iterate all IP addresses assigned to each card...
				for (Enumeration inetAddrs = iface.getInetAddresses(); inetAddrs.hasMoreElements(); ) {
					InetAddress inetAddr = (InetAddress) inetAddrs.nextElement();
					if (!inetAddr.isLoopbackAddress()) {
						if (inetAddr.isSiteLocalAddress()) {
							// Found non-loopback site-local address. Return it immediately...
							String s = inetAddr.toString();
							if (s.contains("/")) s = s.substring(s.indexOf("/") + 1);
							return s;
						} else if (candidateAddress == null) {
							// Found non-loopback address, but not necessarily site-local.
							// Store it as a candidate to be returned if site-local address is not subsequently found...
							candidateAddress = inetAddr;
							// Note that we don't repeatedly assign non-loopback non-site-local addresses as candidates,
							// only the first. For subsequent iterations, candidate will be non-null.
						}
					}
				}
			}
			if (candidateAddress != null) {
				// We did not find a site-local address, but we found some other non-loopback address.
				// Server might have a non-site-local address assigned to its NIC (or it might be running
				// IPv6 which deprecates the "site-local" concept).
				// Return this non-loopback candidate address...
				String s = candidateAddress.toString();
				if (s.contains("/")) s = s.substring(s.indexOf("/") + 1);
				return s;
			}
			// At this point, we did not find a non-loopback address.
			// Fall back to returning whatever InetAddress.getLocalHost() returns...
			InetAddress jdkSuppliedAddress = InetAddress.getLocalHost();
			if (jdkSuppliedAddress == null) {
				Error("The JDK InetAddress.getLocalHost() method unexpectedly returned null.");
			}
			assert jdkSuppliedAddress != null;
			String s = jdkSuppliedAddress.toString();
			if (s.contains("/")) s = s.substring(s.indexOf("/") + 1);
			return s;
		} catch (Exception e) {
			Error("Unknown Host Exception: '" + e.getMessage());
		}
		return null;
	}
	//--------------------------------------------------------------------------
	private static String getMyMACAddress(String ipAddr) {
		byte[] mac = new byte[0];
		try {
			NetworkInterface network = NetworkInterface.getByInetAddress(InetAddress.getByName(ipAddr));
			mac = network.getHardwareAddress();
		} catch (SocketException e) {
			Error("Socket Error: " + e.getMessage());
		} catch (UnknownHostException e) {
			Error("Unknown Host: " + e.getMessage());
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < mac.length; i++) {
			sb.append(String.format("%02X%s", mac[i], (i < mac.length - 1) ? "-" : ""));
		}
		return sb.toString().toLowerCase().replaceAll("-", ":");
	}
	//--------------------------------------------------------------------------
	static void Verbose(String s) {
		if (verboseMode) {
			Calendar cal= Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
			System.out.println(sVerbose + sdf.format(cal.getTime()) + " " + s);
		}
	}
	//--------------------------------------------------------------------------
	static void Error(String s) {
		System.err.println(sError + s);
		Exit(-1);
	}
	//--------------------------------------------------------------------------
	static void Exit(int value) {
		if (netGui.fontConsole != null) netGui.fontConsole.dispose();
		if (netGui.fontButton != null) netGui.fontButton.dispose();
		if (netGui.fontLabel != null) netGui.fontLabel.dispose();
		if (netGui.display != null) netGui.display.dispose();
		if (value == 0) Verbose(sExit);
		System.exit(value);
	}
	//--------------------------------------------------------------------------
	private static boolean validIP(String ip) {
		try {
			if (ip == null || ip.isEmpty()) { return false; }
			String[] parts = ip.split( "\\." );
			if (parts.length != 4) { return false; }
			for (String s : parts) {
				int i = Integer.parseInt(s);
				if ((i < 0) || (i > 255)) { return false; }
			}
			return !ip.endsWith(".");
		} catch (NumberFormatException nfe) {
			return false;
		}
	}
	//--------------------------------------------------------------------------
	private static String grep(String macaddr) {
		String newmacaddr = (macaddr.replace(":", "")).substring(0, 6).toUpperCase();
		BufferedReader br;
		String line = null;
		boolean found = false;
		try {
			br = new BufferedReader(new FileReader(sOuiFile));
			while (true) {
				if ((line = br.readLine()) == null) break;
				if (line.length() == 0) continue;
				if (line.charAt(0) == '\t' || line.charAt(0) == ' ') continue;
				if (! newmacaddr.equals(line.substring(0, 6))) continue;
				found = true;
				break;
			}
			br.close();
		} catch (IOException e) {
			Error("Cannot read '" + sOuiFile + "': " + e.getMessage());
		}
		if (found) {
			String[] parts  = line.split("\\\t");
			if (parts.length != 3) return "";
			return parts[2];
		} else
			return "";
	}
}
//==============================================================================
